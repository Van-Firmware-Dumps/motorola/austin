## user 13 T1SAS33.73-40-5-2-12 8a4e71 release-keys
- Manufacturer: motorola
- Platform: mt6833
- Codename: austin
- Brand: motorola
- Flavor: user
- Release Version: 13
- Kernel Version: 4.19.191
- Id: T1SAS33.73-40-5-2-12
- Incremental: 8a4e71
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: motorola/austin_g/austin:12/T1SAS33.73-40-5-2-12/8a4e71:user/release-keys
- OTA version: 
- Branch: user-13-T1SAS33.73-40-5-2-12-8a4e71-release-keys-11696
- Repo: motorola/austin
